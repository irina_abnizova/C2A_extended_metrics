function [sH12,indd1,indd2,sub_contrib1,sub_contrib2]=main_sub_contributors(thrS,coH1,coH2,subs)
%----------------function main_contributors   


indd1=[];indd2=[];sub_contrib1=[];sub_contrib2=[];

      %----------------------to detect main contributor subs, except Ti
           ind_Ti=[2,6,7,11];
           sH1=signif(coH1);
           sH2=signif(coH2);
           display('contribution of each substitution, R1 and R2');
           sH12=[sH1' sH2']
       
            
           %--------------------get main HQ susbs contributor: per read
           %thrS=1.9;
           sub_contrib1=[];
           sub_contrib2=[];
           k1=0;k2=0;
           for i=1:length(sH1),
               if sH1(i) > thrS & i ~= 2 & i ~= 6 & i ~= 7 & i ~= 11,
                   k1=k1+1;
                   indd1(k1)=i;% indexes: which subs contribute
                   sub_contrib1(k1)=subs(i);
               end
              if sH2(i) > thrS & i ~= 2 & i ~= 6 & i ~= 7 & i ~= 11,
                   k2=k2+1;
                   sub_contrib2(k2)=subs(i);
                   indd2(k2)=i;% indexes: which subs contribute
               end
           end
           
           if k1>0,
           display('contributing substitutions R1')
           sub_contrib1
           else
           display('only Ti might contribute significantly in R1');    
           end
           
           if k2>0,
           display('contributing substitutions R2')
           sub_contrib2
           else
           display('only Ti might contribute significantly in R2');    
           end
     